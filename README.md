
Vanilla 1.14 server with only [CC:Tweaked](https://minecraft.curseforge.com/projects/cc-tweaked)
using the [fabric modloader](http://fabricmc.net/). The server also needs
[fabric API](https://minecraft.curseforge.com/projects/fabric/files).
