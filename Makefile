
.PHONY: install run clean

run: install
	./run-server.sh

install: fabric-server-launch.jar

fabric-server-launch.jar:
	./install-fabric.sh

clean:
	rm -rf .fabric/ .mixin.out/
	rm -f fabric-server-launch.jar
	git checkout -- server.properties
