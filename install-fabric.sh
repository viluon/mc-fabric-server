#!/bin/bash

# Warning: for some reason, the installer always uses the latest version, rather than "1.14+build.21"
java -jar ./fabric-installer.jar server -dir . -mappings "1.14.1+build.5"
